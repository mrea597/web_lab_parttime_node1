var http = require('http');

http.createServer(function(req, res) {
    res.writeHead(200, {"Content": "text/plain"});
    res.end("My name is Martin, and I like ice cream");
}).listen(3000);