var http = require('http');
var path = require('path');

http.createServer(function(req, res) {
    var lookup = path.basename(decodeURI(req.url));

    if (lookup.length == 0) {
        res.writeHead(200, {"Content": "text/html"});
        res.write("<!DOCTYPE html><html>");
        res.write("<head>");
        res.write("<title>Exercise 02</title>");
        res.write("</head>");
        res.write("<body>");
        res.write("<p>My name is Martin, and I like ice cream</p>");
        res.write("</body></html>");
        res.end();
    }
    if (lookup == 'contact') {
        res.writeHead(200, {"Content": "text/html"});
        res.write("<!DOCTYPE html><html>");
        res.write("<head>");
        res.write("<title>Exercise 02</title>");
        res.write("</head>");
        res.write("<body>");
        res.write("<h1>Contact details </h1>");
        res.write("<p>Phone: + 40 999 6541</p>");
        res.write("<p>Email: martin@gmail.com</p>");
        res.write("</body></html>");
        res.end();
    }
    if (!res.finished) {
        res.writeHead(200, {"Content": "text/html"});
        res.end('Page Not Found!');
    }
}).listen(3000);
