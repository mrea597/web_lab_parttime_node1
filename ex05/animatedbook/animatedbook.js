"use strict";

function clearAnimationDelays() {
    var allPages = document.getElementsByClassName("page");
    for (var i = 0; i < allPages.length; i++) {
        allPages[i].style.animationDelay = "0s";
    }
}

function setPageAnimation(pageNum) {
    var pageId = document.getElementById("page" + pageNum);
    pageId.classList.add("pageAnimation");

    pageId.addEventListener("animationend", animationEnded);

    function animationEnded() {
        var nextPage = document.getElementById("page" + (pageNum+1));
        nextPage.style.zIndex = "2";
    }
}



