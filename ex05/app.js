var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/animatedbook/animatedbook.html");
});

app.use(express.static(__dirname + '/animatedbook'));
app.use(express.static(__dirname + '/images'));


// 404 page
app.use(function (req, res) {
    res.type('text/html');
    res.status(404);
    res.send('Page Not Found');
});

app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});